# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda";
  # Point to the encrypted volume
  boot.initrd.luks.devices = [
    {
      name = "root";
      device = "/dev/sda3";
      preLVM = true;
    }
  ];


  # networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true;  # Enables wireless support

  # Set your time zone.
  time.timeZone = "Europe/London";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    chrome-gnome-shell
    chromium
    docker
    docker_compose
    emacs
    firefox
    msmtp
    offlineimap
    okular
    unetbootin
  ];

  # List services that you want to enable:

  services = {

    gnome3 = {
      chrome-gnome-shell.enable = true;
      gnome-keyring.enable = true;
    };

    # Music player daemon
    mpd = {
      enable = true;
      musicDirectory = "/home/garry/Music";
    };

    openssh.enable = true;

    xserver = {
      enable = true;
      libinput.enable = true;
      layout = "gb";

      # Gnome 3 works better with GDM
      displayManager.gdm.enable = true;

      # I want Gnome 3
      desktopManager.gnome3.enable = true;
    };
  };

  nixpkgs.config = {
    allowUnfree = true;
    firefox = {
      enableGnomeExtensions = true;
    };
  };

  # Define user accounts. Don't forget to set a password with ‘passwd’.
  users.extraUsers.garry = {
    name = "Garry";
    group = "users";
    extraGroups = [
      "wheel" "disk" "audio" "video"
      "networkmanager" "systemd-journal"
    ];
    createHome = true;
    uid = 1000;
    home = "/home/garry";
    shell = "/run/current-system/sw/bin/bash";
  };

  users.extraUsers.euan = {
    name = "Euan";
    group = "users";
    extraGroups = [
      "disk" "audio" "video"
      "networkmanager" "systemd-journal"
    ];
    createHome = true;
    home = "/home/euan";
    shell = "/run/current-system/sw/bin/bash";
  };

  # Add some extra fonts
  fonts = {
    enableFontDir = true;
    enableGhostscriptFonts = true;
    fonts = with pkgs; [
      dejavu_fonts
      powerline-fonts
      source-code-pro
      source-sans-pro
      source-serif-pro
      ubuntu_font_family
    ];
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "18.03";

}
