;;; NIX-PRESENATION --- Demo-it demonstration of nix

;;; Commentary:

;; A lightning introduction to the nix language, nix package manager,
;; NixOS and nix DevOps workflow.

;;; Code:

(defun dit-simple-nix-expression ()
  "Show a basic nix expression file."
  (demo-it-presentation-advance)
  (demo-it-load-file "example.nix" nil nil 67))

(defun dit-why-this-matters ()
  "Summary of nix package manager."
  (demo-it-presentation-return))

(defun dit-multiple-versions ()
  "Demonstrate how each package has a hash."
  ;; Close other windows and advance the presentation:
  (demo-it-presentation-return)
  (demo-it-start-shell nil nil nil nil nil 67)
  (demo-it-run-in-shell "ldd /nix/store/*bash*/bin/bash"))

(defun dit-using-your-own-expressions ()
  "Demonstrate the groovy doesn't work."
  ;; Close other windows and advance the presentation:
  (demo-it-presentation-return)
  (demo-it-start-shell nil nil nil nil nil 75)
  (demo-it-run-in-shell "cat example.groovy"))

(defun dit-setting-up-for-nix-shell ()
  "Show a basic nix expression file."
  (demo-it-presentation-return)
  (demo-it-load-file "shell.nix" nil nil 67))

(defun dit-nix-shell-in-action ()
  "Example of a nix-shell command."
  ;; Close other windows and advance the presentation:
  (demo-it-presentation-return)
  (demo-it-start-shell nil nil nil nil nil 67)
  (demo-it-run-in-shell "nix-shell --run 'groovy example.groovy'"))

(defun dit-going-all-in-with-nixos ()
  "Show a full configuration.nix."
  (demo-it-presentation-advance)
  (demo-it-load-file "configuration.nix" nil nil 70))

(defun dit-a-whole-new-system ()
  "Demonstrate a rebuild test."
  (demo-it-presentation-return)
  (demo-it-start-shell nil nil nil nil nil 67)
  (demo-it-run-in-shell "sudo nixos-rebuild test"))

(defun dit-testable-upgrades-painless-rollbacks ()
  "Summary of NixOS advantages."
  (demo-it-presentation-return))

(defun dit-nixops ()
  "Explain what nixops is."
  (demo-it-load-file "load-balancer.nix" nil nil 67))

(defun dit-nixops-deployment-targets ()
  "Explain how deployment targets work."
  (demo-it-presentation-return)
  (demo-it-load-file "load-balancer-ec2.nix" nil nil 67))

(defun dit-nixops-deploy-to-cloud ()
  "Deploy our simple service to AWS."
  (demo-it-presentation-return)
  (demo-it-start-shell nil nil nil nil nil 67)
  (demo-it-run-in-shell "nixops create ./load-balancer.nix ./load-balancer-ec2.nix -d load-balancer-ec2"))

(defun dit-questions ()
  "Invite questions."
  (demo-it-presentation-return))

;; ----------------------------------------------------------------------
;; Demonstration creation and the ordering of steps...

(demo-it-create :advanced-mode :single-window :insert-fast
                :text-large
                (demo-it-title-screen "nix_presentation-title.org")
                (demo-it-presentation "nix_presentation.org")
                dit-simple-nix-expression ;;                Step 3
                dit-why-this-matters ;;                     Step 4
                dit-multiple-versions ;;                    Step 5
                dit-using-your-own-expressions ;;           Step 6
                dit-setting-up-for-nix-shell ;;             Step 7
                dit-nix-shell-in-action ;;                  Step 8
                dit-going-all-in-with-nixos ;;              Step 9
                dit-a-whole-new-system ;;                   Step 10
                dit-testable-upgrades-painless-rollbacks ;; Step 11
                dit-nixops ;;                               Step 12
                dit-nixops-deployment-targets ;;            Step 13
                dit-nixops-deploy-to-cloud ;;               Step 14
                dit-questions ;;                            Step 15
                )

;; ----------------------------------------------------------------------
;; Start the presentation whenever this script is evaluated. Good idea?

(demo-it-start)

;;; nix_presentation.el ends here
