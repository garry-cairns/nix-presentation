{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "base_groovy_development";
    buildInputs = [
      pkgs.groovy
    ];
  }
